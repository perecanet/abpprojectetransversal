<mxfile host="app.diagrams.net" modified="2020-09-30T14:13:38.941Z" agent="5.0 (X11)" etag="kWeKtwx8vRahND9zE4Hf" version="13.7.6" type="gitlab">
  <diagram id="7eJPsrMMGlFUblSFdccf" name="Page-1">
    <mxGraphModel dx="1422" dy="773" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169" math="0" shadow="0">
      <root>
        <mxCell id="0" />
        <mxCell id="1" parent="0" />
        <mxCell id="L1qqJIfydtukJoNoo9L2-11" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="1" source="L1qqJIfydtukJoNoo9L2-1" target="L1qqJIfydtukJoNoo9L2-2" edge="1">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="510" y="190" />
              <mxPoint x="370" y="190" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="L1qqJIfydtukJoNoo9L2-17" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="1" source="L1qqJIfydtukJoNoo9L2-1" target="L1qqJIfydtukJoNoo9L2-12" edge="1">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="510" y="190" />
              <mxPoint x="130" y="190" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="sdfPYX9wQXbc6r1BSc-U-6" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="1" source="L1qqJIfydtukJoNoo9L2-1" target="sdfPYX9wQXbc6r1BSc-U-1" edge="1">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="510" y="190" />
              <mxPoint x="650" y="190" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="dsaEiTBdKk52ZvkwPAS2-2" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" edge="1" parent="1" source="L1qqJIfydtukJoNoo9L2-1" target="sdfPYX9wQXbc6r1BSc-U-2">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="510" y="190" />
              <mxPoint x="850" y="190" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="L1qqJIfydtukJoNoo9L2-1" value="&lt;div&gt;Pàgina&lt;/div&gt;&lt;div&gt;Principal&lt;br&gt;&lt;/div&gt;" style="ellipse;shape=cloud;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="450" y="80" width="120" height="80" as="geometry" />
        </mxCell>
        <mxCell id="L1qqJIfydtukJoNoo9L2-10" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="1" source="L1qqJIfydtukJoNoo9L2-2" target="L1qqJIfydtukJoNoo9L2-3" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="L1qqJIfydtukJoNoo9L2-2" value="&lt;div&gt;Pàgina &lt;br&gt;&lt;/div&gt;&lt;div&gt;Inici Sessió&lt;br&gt;&lt;/div&gt;" style="rounded=1;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="310" y="230" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="L1qqJIfydtukJoNoo9L2-6" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="1" source="L1qqJIfydtukJoNoo9L2-3" target="L1qqJIfydtukJoNoo9L2-4" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="L1qqJIfydtukJoNoo9L2-3" value="Pàgina Registre" style="rounded=1;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="310" y="370" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="L1qqJIfydtukJoNoo9L2-4" value="&lt;div&gt;Pàgina Verificació&lt;/div&gt;&lt;div&gt;Correu&lt;br&gt;&lt;/div&gt;" style="rounded=1;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="310" y="525" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="L1qqJIfydtukJoNoo9L2-5" value="&lt;div&gt;Pàgina Verificació&lt;/div&gt;&lt;div&gt;Família Necessitada&lt;br&gt;&lt;/div&gt;" style="rounded=1;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="460" y="450" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="L1qqJIfydtukJoNoo9L2-7" value="" style="endArrow=none;dashed=1;html=1;dashPattern=1 3;strokeWidth=2;exitX=0.995;exitY=0.576;exitDx=0;exitDy=0;exitPerimeter=0;entryX=0.538;entryY=-0.021;entryDx=0;entryDy=0;entryPerimeter=0;" parent="1" source="L1qqJIfydtukJoNoo9L2-3" target="L1qqJIfydtukJoNoo9L2-5" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="500" y="410" as="sourcePoint" />
            <mxPoint x="550" y="360" as="targetPoint" />
            <Array as="points">
              <mxPoint x="525" y="405" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="L1qqJIfydtukJoNoo9L2-9" value="" style="endArrow=none;dashed=1;html=1;dashPattern=1 3;strokeWidth=2;exitX=0.995;exitY=0.576;exitDx=0;exitDy=0;exitPerimeter=0;entryX=0.5;entryY=1;entryDx=0;entryDy=0;" parent="1" target="L1qqJIfydtukJoNoo9L2-5" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="430.0000000000001" y="550" as="sourcePoint" />
            <mxPoint x="525.1600000000002" y="594.1800000000001" as="targetPoint" />
            <Array as="points">
              <mxPoint x="520" y="550" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="dsaEiTBdKk52ZvkwPAS2-3" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" edge="1" parent="1" source="L1qqJIfydtukJoNoo9L2-12" target="dsaEiTBdKk52ZvkwPAS2-1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="L1qqJIfydtukJoNoo9L2-12" value="&lt;div&gt;Pàgina&lt;/div&gt;&lt;div&gt;Restaurants/&lt;/div&gt;&lt;div&gt;Venedors&lt;br&gt;&lt;/div&gt;" style="rounded=1;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="70" y="230" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="L1qqJIfydtukJoNoo9L2-14" value="&lt;div&gt;Pàgina&lt;/div&gt;&lt;div&gt;Pagament&lt;br&gt;&lt;/div&gt;" style="rounded=1;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="70" y="525" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="sdfPYX9wQXbc6r1BSc-U-8" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;entryX=0.45;entryY=-0.067;entryDx=0;entryDy=0;entryPerimeter=0;" parent="1" source="sdfPYX9wQXbc6r1BSc-U-1" target="sdfPYX9wQXbc6r1BSc-U-3" edge="1">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="650" y="550" />
              <mxPoint x="564" y="550" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="sdfPYX9wQXbc6r1BSc-U-9" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="sdfPYX9wQXbc6r1BSc-U-1" target="sdfPYX9wQXbc6r1BSc-U-4" edge="1">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="650" y="550" />
              <mxPoint x="770" y="550" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="sdfPYX9wQXbc6r1BSc-U-1" value="&lt;div&gt;Pàgina&lt;/div&gt;&lt;div&gt;Usuari&lt;/div&gt;&lt;div&gt;Consumidor&lt;br&gt;&lt;/div&gt;" style="rounded=1;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="590" y="230" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="sdfPYX9wQXbc6r1BSc-U-10" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="1" source="sdfPYX9wQXbc6r1BSc-U-2" target="sdfPYX9wQXbc6r1BSc-U-5" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="sdfPYX9wQXbc6r1BSc-U-2" value="&lt;div&gt;Pàgina&lt;/div&gt;&lt;div&gt;Usuari&lt;/div&gt;&lt;div&gt;Venedor&lt;br&gt;&lt;/div&gt;" style="rounded=1;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="790" y="230" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="sdfPYX9wQXbc6r1BSc-U-3" value="&lt;div&gt;Pàgina&lt;/div&gt;&lt;div&gt;Descomptes&lt;br&gt;&lt;/div&gt;" style="rounded=1;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="510" y="600" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="sdfPYX9wQXbc6r1BSc-U-4" value="&lt;div&gt;Pàgina&lt;/div&gt;&lt;div&gt;Identificació&lt;/div&gt;&lt;div&gt;Usuari Necessitat&lt;br&gt;&lt;/div&gt;" style="rounded=1;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="710" y="600" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="sdfPYX9wQXbc6r1BSc-U-5" value="&lt;div&gt;Pàgina&lt;/div&gt;&lt;div&gt;Venta&lt;br&gt;&lt;/div&gt;" style="rounded=1;whiteSpace=wrap;html=1;" parent="1" vertex="1">
          <mxGeometry x="790" y="370" width="120" height="60" as="geometry" />
        </mxCell>
        <mxCell id="dsaEiTBdKk52ZvkwPAS2-4" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" edge="1" parent="1" source="dsaEiTBdKk52ZvkwPAS2-1" target="L1qqJIfydtukJoNoo9L2-14">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="dsaEiTBdKk52ZvkwPAS2-1" value="&lt;div&gt;Pàgina&lt;/div&gt;&lt;div&gt;Restaurant/&lt;/div&gt;&lt;div&gt;Venedor&lt;br&gt;&lt;/div&gt;" style="rounded=1;whiteSpace=wrap;html=1;" vertex="1" parent="1">
          <mxGeometry x="70" y="370" width="120" height="60" as="geometry" />
        </mxCell>
      </root>
    </mxGraphModel>
  </diagram>
</mxfile>
