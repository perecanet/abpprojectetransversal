# PROJECTE ABP TRANSVERSAL

## De què tracta el projecte?

El nostre projecte tracta d'una aplicació web de menjar a domicili, la qual implementa l'opció de que els restaurants ofereixin 'lotes'
de menjar amb caducitat propera per tal d'evitar el desaprofitament de menjar que es fa avui en dia. La web porta el nom de 'NoLeftovers' .
