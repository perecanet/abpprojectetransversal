<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RestaurantsSeeder::class);
    }
}

class RestaurantsSeeder extends Seeder
{
    /**S
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurants')->insert([
            'name'=> 'Can Xapas',
            'image'=> 'canxapes.jpg',
            'location'=> 'Carrer València, 145, Barcelona',
            'type'=> 'Mexica',
            'menu'=> 1,
            'discount'=> 1,
            'lots'=> 1,
        ]);

        DB::table('restaurants')->insert([
            'name'=> 'Casa Manoli',
            'image'=> 'canamanoli.jpg',
            'location'=> 'Carrer Tarradelles, 14, Barcelona',
            'type'=> 'Vegetarià',
            'menu'=> 1,
            'discount'=> 0,
            'lots'=> 1,
        ]);

        DB::table('restaurants')->insert([
            'name'=> 'McDonalds',
            'image'=> 'mcdonalds.jpg',
            'location'=> 'Carrer Collbanc, 234, Barcelona',
            'type'=> 'Americà',
            'menu'=> 1,
            'discount'=> 1,
            'lots'=> 0,
        ]);

        DB::table('restaurants')->insert([
            'name'=> 'AmeRestaurant',
            'image'=> 'ame.jpg',
            'location'=> 'Avinguda Diagonal, 321, Barcelona',
            'type'=> 'Mexica',
            'menu'=> 0,
            'discount'=> 0,
            'lots'=> 1,
        ]);

        DB::table('restaurants')->insert([
            'name'=> 'Timesburg',
            'image'=> 'timesburg.jpg',
            'location'=> 'Avinguda Diagonal, 301, Barcelona',
            'type'=> 'Americà',
            'menu'=> 0,
            'discount'=> 0,
            'lots'=> 0,
        ]);

    }
}

