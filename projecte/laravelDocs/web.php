<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RestaurantsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('restaurants', [RestaurantsController::class, 'list']);
Route::post('restaurants', [RestaurantsController::class, 'listSearch']);
Route::get('restaurants/descomptes', [RestaurantsController::class, 'listDescomptes']);
Route::get('restaurants/menus', [RestaurantsController::class, 'listMenus']);
Route::get('restaurants/lotes', [RestaurantsController::class, 'listLotes']);
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('restaurants/tipus', [RestaurantsController::class,'listTipus']);

Route::get('nouproducte', function () {
    return view('newProduct');
});

Auth::routes();
